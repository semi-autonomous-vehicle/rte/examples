

ARCOMPILER = java -jar /opt/artext-demo/artext-compiler-1.5.1-SNAPSHOT.jar -ar Autosar40
OUT_DIR = ../autosar_compiled


SWCD_FILES = $(shell find . -type f -name '*.swcd')
ARXML_FILES = $(patsubst ./%.swcd, $(OUT_DIR)/%.arxml, $(SWCD_FILES))


.PHONY: all

all: $(ARXML_FILES)

dbg:
	@echo $(SWCD_FILES)
	@echo $(ARXML_FILES)

$(OUT_DIR)/%.arxml: ./%.swcd
	@mkdir -p "$(@D)"
	@echo convert "$<" to "$@"
	-@$(ARCOMPILER) -outputFile $@ $<
